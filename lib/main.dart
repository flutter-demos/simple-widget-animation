import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Simple Widget Animation'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double _width = 300;
  double _height = 300;

  Color _color = Colors.amber[600];
  BorderRadiusGeometry _borderRadius = BorderRadius.circular(8);

  void _updateState() {
    setState(() {
      final random = Random();

      // width, height = min + random.nextInt(max - min) to get range [min, max]
      _width = 100 + random.nextInt(300 - 100).toDouble();
      _height = 100 + random.nextInt(300 - 100).toDouble();

      _borderRadius = BorderRadius.circular(random.nextInt(100).toDouble());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: InkWell(
          onTap: () {
            _updateState();
          },
          child: AnimatedContainer(
            duration: Duration(milliseconds: 850),
            curve: Curves.elasticOut,
            margin: const EdgeInsets.all(10.0),
            decoration:
                BoxDecoration(color: _color, borderRadius: _borderRadius),
            width: _width,
            height: _height,
            child: Center(
              child: Text('Animate me'),
            ),
          ),
        ),
      ),
    );
  }
}
